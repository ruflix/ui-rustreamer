/**
 * TCD Software
 * Created by Dmitrij Rysanow on 08.02.17.
 */
(function() {
    angular.module('app.player')
        .directive('player', player)
        .constant('PLAYER_GLOBAL_CONTROLS', {
            OPEN_NOW_PLAYING: 'OPEN_NOW_PLAYING',
            OPEN_FULLSCREEN: 'OPEN_FULLSCREEN',
            CLOSE_NOW_PLAYING: 'CLOSE_NOW_PLAYING'
        });
    /**
     * @ngInject
     * @param {PlayerService} PlayerService
     * @param {*} $state
     * @param {ngPopover} ngPopover
     * @param {!angular.scope} $rootScope
     * @param {!angular.timeout} $timeout
     * @param {angular.constant} PLAYER_GLOBAL_CONTROLS
     * @return {angular.directive}
     */
    function player(PlayerService,
                    $state,
                    ngPopover,
                    $rootScope,
                    $timeout,
                    PLAYER_GLOBAL_CONTROLS,
                    PLAYING_STATUS,
                    $mdToast) {
        return {
            restrict: 'E',
            templateUrl: 'app/player/templates/player.html',
            link: function(scope) {
                'use strict';
                scope.enabled = false;
                var lockSlider = false;
                scope.playingStatusEnum = PLAYING_STATUS;
                scope.playerStatus = {
                    track: {
                        name: '',
                        length: 0
                    }
                };

                scope.play = PlayerService.unpause;
                scope.pause = PlayerService.pause;
                scope.next = PlayerService.nextTrack;
                scope.prev = PlayerService.prevTrack;
                scope.volume = 100;
                scope.position = 0;
                scope.video = null;
                updateCurrentState();

                function updateCurrentState() {
                    scope.currentState = $state.current.name;
                }

                scope.openNowPlaying = function() {
                    if (scope.playerStatus.getCurrentTrack().mime.indexOf('video') > -1) {
                        $state.go('app.video');
                        scope.currentState = 'app.video';
                    } else {
                        $state.go('app.nowplaying');
                        scope.currentState = 'app.nowplaying';
                    }
                };

                scope.closeNowPlaying = function() {
                    $rootScope.$broadcast(PLAYER_GLOBAL_CONTROLS.CLOSE_NOW_PLAYING);
                    $timeout(updateCurrentState);
                };

                scope.unlockPositionSlider = function() {
                    lockSlider = true;
                    PlayerService.setPosition(scope.position);
                    scope.$evalAsync(function() {
                        lockSlider = false;
                    }.bind(this));
                };

                scope.closeVolume = function() {
                    $timeout(ngPopover.close, 200);
                };

                scope.fullscreen = function() {
                    $rootScope.$broadcast(PLAYER_GLOBAL_CONTROLS.OPEN_FULLSCREEN);
                };

                scope.$watch('volume', function(newVal) {
                    PlayerService.setVolume(newVal);
                });

                /**
                 * @implements {IAbstractPlayer}
                 * @constructor
                 */
                function PlayerListener() {}
                PlayerListener.prototype.updateStatus = function(status) {
                    scope.playerStatus = status;
                    if (status.getCurrentTrack() && !lockSlider) {
                        scope.position = status.position;
                    }
                };
                PlayerListener.prototype.activate = function() {
                    scope.enabled = true;
                };
                PlayerListener.prototype.deactivate = function() {
                    scope.enabled = false;
                };
                PlayerListener.prototype.requestVideo = function() {
                    $state.go('app.video');
                    scope.currentState = 'app.video';
                };
                PlayerListener.prototype.requestAudio = function() {
                    updateCurrentState();
                };
                PlayerListener.prototype.error = function (message) {
                    console.log("error ", message);
                    $mdToast.show($mdToast.simple().textContent("Could not play this file"));
                };
                function init() {
                    PlayerService.addListener(PlayerListener);
                }

                init();
            }
        };
    }
})();
