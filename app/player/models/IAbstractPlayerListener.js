(function() {
    angular.module('app.player')
        .service('IAbstractPlayerListener', IAbstractPlayerListener);

    /**
    * @interface
    * @export
    * @return {IAbstractPlayerListener} events interface
    */
    function IAbstractPlayerListener() {}
        /**
         * Calls when Player activates
         */
        IAbstractPlayerListener.prototype.activate = function() {};
        /**
         * Calls every PlayerService.UPDATE_INTERVAL, returning status object
         * (see PlayerService.getStatusObject)
         * @param {*} status
         */
        IAbstractPlayerListener.prototype.updateStatus = function(status) {};

        IAbstractPlayerListener.prototype.requestVideo = function() {};

        IAbstractPlayerListener.prototype.requestAudio = function() {};

        IAbstractPlayerListener.prototype.error = function () {};

        IAbstractPlayerListener.prototype.deactivate = function () {};

        return IAbstractPlayerListener;
})();
