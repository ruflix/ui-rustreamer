/**
 * TCD Software
 * Created by Dmitrij Rysanow on 02.03.17.
 */
(function() {
    angular.module('app.player', [
        'ui.router',
        'ngMaterial',
        'ngMdIcons',
        'angular-loading-bar',
        'ngPopover',
        'ngAnimate',
        'app.common',
        'app.auth']);
})();
