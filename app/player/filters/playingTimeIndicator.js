/**
 * TCD Software
 * Created by Dmitrij Rysanow on 19.02.17.
 */
(function() {
    angular.module('app.player')
        .filter('playingTimeIndicator', playingTimeIndicator);

    function playingTimeIndicator() {
        return function(seconds) {
            return new Date(0, 0, 0).setSeconds(Math.floor(seconds));
        };
    }
})();
