/**
 * Created by tcd on 04.03.17.
 */
(function() {
    'use strict';
    angular.module('app.player')
        .controller('VideoController', VideoController);

    /**
     *
     * @ngInject
     * @export
     * @constructor
     */
    function VideoController(PlayerService,
                             $scope,
                             APP_EVENTS,
                             PreviousState,
                             $state,
                             $rootScope,
                             PLAYER_GLOBAL_CONTROLS,
                             $mdSidenav) {
        var vm = this;
        var element = document.getElementById('video');
        vm.toggleSidenav = toggleSidenav;
        vm.back = back;
        vm.fullscreen = fullscreen;
        vm.toggleOptions = toggleOptions;
        vm.playlist = PlayerService.getPlaylist();
        vm.playTrack = PlayerService.jumpToTrackOnPlaylist;

        $scope.$emit(APP_EVENTS.REQUEST_MOBILE_LEFTBAR);
        $scope.$emit(APP_EVENTS.REQUEST_CLOSE_LEFTBAR);
        var closeNowPlayingListener = $rootScope.$on(PLAYER_GLOBAL_CONTROLS.CLOSE_NOW_PLAYING, back);
        var openFullscreenListener = $rootScope.$on(PLAYER_GLOBAL_CONTROLS.OPEN_FULLSCREEN, fullscreen);
        PlayerService.bindVideo(element);


        $scope.$on('$destroy', function() {
            console.log('destroy player events');
            $scope.$emit(APP_EVENTS.REQUEST_DEFAULT_LEFTBAR);
            closeNowPlayingListener();
            openFullscreenListener();
        });

        function back() {
            if ($state.current.name === 'app.video') {
                $scope.$emit(APP_EVENTS.REQUEST_DEFAULT_LEFTBAR);
                PlayerService.pause();
                $state.go(PreviousState.Name, PreviousState.Params);
            }
        }

        function toggleSidenav() {
            $scope.$emit(APP_EVENTS.REQUEST_TOGGLE_LEFTBAR);
        }

        function fullscreen() {
            if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            }
        }

        function playAudioTrack(index) {
            console.log('Audiotrack switched to ' + vm.playerStatus.track.metadata.streams.tags.title);
        }

        function toggleOptions() {
            $mdSidenav('right')
          .toggle()
          .then(function () {
            console.log('opened');
          });
        }

        function PlayerListener() {}
        PlayerListener.prototype.updateStatus = function(status) {
            vm.playerStatus = status;
        };
        PlayerListener.prototype.activate = function() {
            vm.captions = element.textTracks;
        };
        PlayerListener.prototype.deactivate = function() {
            back();
        };


        function init() {
            PlayerService.addListener(PlayerListener);
        }

        init();
    }
})();
