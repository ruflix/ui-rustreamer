/**
 * TCD Software
 * Created by Dmitrij Rysanow on 05.11.16.
 */
(function() {
    'use strict';

    angular
        .module('app')
        .config(function($stateProvider,
                     $urlRouterProvider,
                     RutrackerAPIProvider,
                     $httpProvider, $mdThemingProvider) {
        'use strict';
        $mdThemingProvider.theme('default')
            .primaryPalette('grey')
            .accentPalette('pink');
        $stateProvider
            .state('splash', {
                url: '/splash',
                templateUrl: 'app/tpl/splash.html',
                controller: 'SplashScreenController'
            })
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/tpl/app.html',
                controller: 'AppController',
                controllerAs: 'vm'
            });


        $httpProvider.defaults.cache = true;

        $urlRouterProvider.otherwise('/splash');
    })
})();
