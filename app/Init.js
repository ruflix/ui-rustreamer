/**
 * TCD Software
 * Created by Dmitrij Rysanow on 16.11.16.
 */
(function () {
    'use strict';
    angular
        .module('app', [
            'ui.router',
            'ngMaterial',
            'ngMdIcons',
            'angular-loading-bar',
            'ngPopover',
            'ngAnimate',
            'app.common',
            'app.player',
            'app.browse',
            'app.auth',
            'app.settings',
            'pascalprecht.translate'])
        .config(function ($translateProvider, $compileProvider) {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob|chrome-extension):|data:image\/)/);
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|chrome-extension):/);
            $translateProvider
                .registerAvailableLanguageKeys(['en_US', 'pl_PL'])
                .preferredLanguage('pl_PL')
                .useStaticFilesLoader({
                    prefix: 'app/i18n/locale-',
                    suffix: '.json'
                })
                .forceAsyncReload(true)
                .fallbackLanguage('en_US');
        })
        .run(function ($location) {
            $location.url('/splash');

        });
})();
