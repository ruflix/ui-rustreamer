/**
 * Created by kinia on 09.06.17.
 */
(function () {
    angular.module('app.common')
        .animation('.slideUp', slideUp);
    function slideUp() {
        return {
            addClass: function (element, className, done) {
                if (className !== "ng-hide") {
                    done();
                    return;
                }
                TweenLite.fromTo(element, 0.5, {x: -100},
                    {
                        autoAlpha:0,
                        x:0,
                        onComplete: done
                    });
            }
        }
    }
});