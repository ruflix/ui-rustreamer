/**
 * Created by kinia on 08.06.17.
 */
(function () {
    angular.module('app.common')
        .animation('.listIn', listIn);
    function listIn() {
        return {
            enter: function (element, done) {
                TweenLite.fromTo(element, 0.2, {scale: 0}, {scale:1, ease: Power2.easeIn, onComplete: done});
            }
        }
    }
})();