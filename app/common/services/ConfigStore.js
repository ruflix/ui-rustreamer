/**
 * TCD Software
 * Created by Dmitrij Rysanow on 15.03.17.
 */
(function() {
    angular.module('app.common')
        .factory('ConfigStore', ConfigStore);
    /**
     *
     * @constructor
     * @export
     */
    function ConfigStore() {
        var _config;
        return {
            get: function() {
                return _config;
            },
            set: function(config) {
                _config = config;
            }
        };
    }
})();
