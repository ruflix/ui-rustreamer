/**
 * TCD Software
 * Created by Dmitrij Rysanow on 29.12.16.
 */
(function() {
    angular.module('app.common')
        .factory('Loki', Loki);
    /**
     * Contains database objects
     * @param {angular.$q} $q
     * @constructor
     * @ngInject
     */
    function Loki($q) {
        'use strict';
        var db = {};
        var feed, categories, forums, langs, lastSync, likes;
        var configObject = {
            autosave: false
        };

        function onDBLoaded(callback) {
            feed = db.getCollection('feed') ||
                db.addCollection('feed', {unique: ['id']});
            categories = db.getCollection('categories') ||
                db.addCollection('categories', {unique: ['id']});
            forums = db.getCollection('forums') ||
                db.addCollection('forums', {unique: ['id']});
            langs = db.getCollection('langs') ||
                db.addCollection('langs', {unique: ['id']});
            lastSync = db.getCollection('lastSync') ||
                db.addCollection('lastSync');
            likes = db.getCollection('likes') ||
                db.addCollection('likes', {unique: ['id']});
            console.log('LokiJS ready');
            callback();
        }

        return {
            init: function() {
                var defer = $q.defer();
                db = new loki('localDB', configObject);
                db.loadDatabase({}, function() {
                    onDBLoaded(defer.resolve);
                });
                return defer.promise;
            },
            /**
             * Saves database to it's resource
             * @return {Promise}
             */
            saveChanges: function() {
                var defer = $q.defer();
                console.log('LokiJS saving changes');
                db.saveDatabase(defer.resolve);
                return defer.promise;
            },
            /**
             * Gets feed DataObject
             * @return {*}
             */
            getFeedDTO: function() {
                return feed;
            },
            /**
             * Gets categories DataObject
             * @return {*}
             */
            getCategoriesDTO: function() {
                return categories;
            },
            /**
             * Gets forums DataObject
             * @return {*}
             */
            getForumsDTO: function() {
                return forums;
            },
            /**
             * Gets forums DataObject
             * @return {*}
             */
            getLastSyncDTO: function() {
                return lastSync;
            },
            /**
             * Gets forums DataObject
             * @return {*}
             */
            getLangsDTO: function() {
                return langs;
            },
            /**
             * Get likes
             * @returns {*}
             */
            getLikesDTO: function() {
                return likes;
            }
        };
    }
})();
