/**
 * TCD Software
 * Created by Dmitrij Rysanow on 12.03.17.
 */
(function() {
    angular.module('app.common')
        .factory('LoadBackend', LoadBackend);

    /**
     *
     * @param {Environment} Environment
     * @param {ENVIRONMENTS} ENVIRONMENTS
     * @param {angular.q} $q
     * @param {ConfigStore} ConfigStore
     * @param {angular.http} $http
     * @ngInject
     * @constructor
     * @export
     */
    function LoadBackend(Environment, ENVIRONMENTS, $q, ConfigStore, $http) {
        /**
         * in NWJS env we have direct access to node
         * so we can run backend from here
         * @param {q.defer} defer
         */
        function configureAsNWJS(defer) {
            var backend = require('backend-rustreamer');
            backend.run({
                hostFront: false,
                frontDir: null
            }).then(function(config) {
                ConfigStore.set(config);
                defer.resolve();
            });
        }

        function configureAsBrowser(defer) {
            //in browser we are shure that backend
            // already works and hosted this client
            return $http.get('getconfig')
                .then(function(config) {
                    ConfigStore.set(config.data);
                    defer.resolve();
                }.bind(this));
        }

        function run() {
            var defer = $q.defer();
            var current = Environment.getCurrent();
            if (current === ENVIRONMENTS.NWJS) {
                configureAsNWJS(defer);
            } else {
                configureAsBrowser(defer);
            }
            return defer.promise;
        }
        return {
            run: run
        };
    }
})();
