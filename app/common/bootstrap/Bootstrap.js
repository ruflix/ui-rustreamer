/**
 * TCD Software
 * Created by Dmitrij Rysanow on 29.12.16.
 */
(function() {
    angular.module('app.common')
        .factory('Bootstrap', Bootstrap);
    /**
     * Helps app bootstrap, run returns the promise
     *
     * @todo validate fallbacks (onFailed)
     * @param {Loki} Loki
     * @param {LoadBackend} LoadBackend
     * @param {PreloadData} PreloadData
     * @constructor
     * @ngInject
     * @export
     */
    function Bootstrap(Loki, LoadBackend, PreloadData) {
        'use strict';
        return {
            run: function() {
                console.log('Bootstrap app');
                return Loki.init()
                    .then(LoadBackend.run)
                    .then(PreloadData.run);
            }
        };
    }
})();
