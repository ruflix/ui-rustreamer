/**
 * Created by kinia on 18.06.17.
 */
(function () {
    angular.module('app.common')
        .directive('animatedLogo', animatedLogo);

    function animatedLogo() {
        return {
            template: '<img src="/app/images/logo.png" class="logo">',
            restrict: 'E',
            transclude: true,
            link: function (scope, element) {
                var image = element[0].querySelector('img');
                var tl = new TimelineMax({repeat: -1});
                tl
                    .to(image, 0.5, {css: {
                        rotation: 360,
                        transformOrigin: 'center center'
                    }, ease: Power0.easeNone, force3D: true, repeat: -1}, 0)
                    .to(image, 1, {scale:1.4, ease: Elastic.easeOut.config(1, 0.5), repeat: -1}, 2);

            }
        }
    }
})();