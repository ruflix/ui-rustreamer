/**
 * TCD Software
 * Created by Dmitrij Rysanow on 25.12.16.
 */
(function () {
    angular.module('app.common')
        .directive('musicitem', MusicItem);
    /**
     *
     * @param {PlayerService} PlayerService
     * @return {{restrict: string, scope: string, templateUrl: string, link: link}}
     */
    function MusicItem(PlayerService) {
        'use strict';
        return {
            restrict: 'A',
            scope: '&',
            templateUrl: 'app/common/directives/torrentItems/musicItem/musicitem.html',
            link: function(scope, element, attr) {
                scope.DEFAULT_ICON = 'music_note';
                scope.PLAY_ICON = 'play_arrow';
                scope.INFO_ICON = 'info_outline';
                scope.PAUSE_ICON = 'pause';

                scope.currentIcon = scope.DEFAULT_ICON;

                scope.switchIcon = function(icon) {
                    scope.currentIcon = icon;
                };

                scope.play = function($event) {
                    $event.stopPropagation();
                    PlayerService.startStream(scope.entry);
                };
            }
        };
    }
})();
