/**
 * TCD Software
 * Created by Dmitrij Rysanow on 25.12.16.
 */
(function() {
    angular.module('app.common')
        .directive('torrentItemCard', torrentItemCard);
    /**
     *
     * @param {angular.compile} $compile
     * @return {angular.directive}
     * @export
     * @ngInject
     */
    function torrentItemCard($compile) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                type: '=',
                entry: '='
            },
            link: function(scope, $element) {
                var type = scope.type || scope.entry.type;
                var el = $compile('<md-card class="torrentItem hoverEffect scale" ' + type + 'item></md-card>')(scope);
                $element.append(el);
            }
        };
    }
})();

