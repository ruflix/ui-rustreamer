/**
 * Created by tcd-primaris on 04.06.17.
 */
(function () {
    angular.module('app.common')
        .factory('PlayerStatus', PlayerStatus)
        .constant('PLAYING_STATUS', {
            PLAYING: 'PLAYING',
            PAUSED: 'PAUSED',
            BUFFERING: 'BUFFERING',
            STOPPED: 'STOPPED'
        });

    /**
     * @ngInject
     * @param PLAYING_STATUS
     * @returns {PlayerStatus}
     * @constructor
     */
    function PlayerStatus(PLAYING_STATUS) {
        /**
         * @param args
         * @constructor
         * @export
         */
        function PlayerStatus(args) {
            this.position = 0;
            this.torrentId = null;
            this.currentTrackNumber = 0;
            this.duration = 0;
            this.playlist = [];
            this.statusText = PLAYING_STATUS.STOPPED;
            this.item = null;

            if (args) {
                var fields = Object.keys(this);
                for (var i in fields) {
                    if (args.hasOwnProperty(fields[i])) {
                        this[fields[i]] = args[fields[i]];
                    }
                }
            }
        }

        PlayerStatus.prototype.getCurrentTrack = function() {
            if (this.currentTrackNumber > this.playlist.length) {
                this.currentTrackNumber = 0;
            }
            return this.playlist[this.currentTrackNumber];
        };

        return PlayerStatus;
    }

})();