/**
 * TCD Software
 * Created by Dmitrij Rysanow on 14.03.17.
 */
(function() {
    angular.module('app.common')
        .config(EndpointsConfig);
    /**
     * @param {angular.provider} RutrackerAPIProvider
     * @param {angular.provider} EnvironmentProvider
     * @param {angular.constant} ENVIRONMENTS
     * @constructor
     * @ngInject
     */
    function EndpointsConfig(RutrackerAPIProvider,
                             EnvironmentProvider,
                             ENVIRONMENTS) {
        if (EnvironmentProvider.getCurrent() === ENVIRONMENTS.NWJS) {
            RutrackerAPIProvider.setEndpoint('http://localhost:7000');
        } else if (EnvironmentProvider.getCurrent() === ENVIRONMENTS.Browser) {
            RutrackerAPIProvider.setEndpoint('');
        }
    }
})();
