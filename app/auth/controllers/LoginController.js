/**
 * TCD Software
 * Created by Dmitrij Rysanow on 16.11.16.
 */
(function () {
    'use strict';
    angular.module('app.auth')
        .controller('LoginController', LoginController);
    /**
     * Login controller
     *
     * @param {!angular.scope} $scope
     * @param {$state} $state
     * @param {$mdToast} $mdToast
     * @param {AuthService} AuthService
     * @constructor
     * @ngInject
     * @export
     */
    function LoginController($scope, $state, $mdToast, AuthService) {
        $scope.username = null;
        $scope.password = null;
        $scope.login = function() {
            AuthService.makeSession({
                username: $scope.username,
                password: $scope.password
            }).then(function () {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Welcome, ' + $scope.username)
                        .hideDelay(3000)
                );
                $state.go('app.browse');
            }, function () {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Cannot login')
                        .hideDelay(3000)
                );
            });
        };
    }
})();
