/**
 * TCD Software
 * Created by Dmitrij Rysanow on 02.03.17.
 */
(function() {
    'use strict';
    angular.module('app.auth', [
        'ui.router',
        'ngMaterial',
        'ngMdIcons',
        'angular-loading-bar',
        'ngPopover',
        'ngAnimate',
        'app.common'])
})();

