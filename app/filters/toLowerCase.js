/**
 * Created by kinia on 14.05.17.
 */
(function () {
    angular.module('app')
        .filter('toLowerCase', toLowerCase);

    function toLowerCase () {
        'use strict';
        return function (str) {
            return str.toLowerCase();
        }
    }
})();