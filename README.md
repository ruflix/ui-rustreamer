**ui-rustreamer**

UI-Part of Ruflix project. Based on AngularJS Framework

It contains:

- Reading categories and torrents from rutracker, also reading feed
- Global search
- Playing music from rutracker
- Playing video (in implementation)


In future:

- User Settings
- Subtitles and audio switch for videos
- Better design
- Downloads view
- Media library view
- many other options

Project is in active development, and first alpha 
(0.0.1) version is planned for Nov. 2017